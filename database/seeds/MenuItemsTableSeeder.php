<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class MenuItemsTableSeeder extends Seeder
{
    public function run()
    {
        Eloquent::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('ussd_menu_items')->truncate();

        DB::table('ussd_menu_items')->delete();

        DB::table('ussd_menu_items')->insert(array(
            array(
                'menu_id' => 1,
                'description' => 'Register',
                'next_menu_id' => 3,
                'step' => 0,
                'confirmation_phrase' => '',
            ),
            array(
                'menu_id' => 1,
                'description' => 'Cancel',
                'next_menu_id' => 1,
                'step' => 0,
                'confirmation_phrase' => '',
            ),
            array(
                'menu_id' => 2,
                'description' => 'Request Loan',
                'next_menu_id' => 2,
                'step' => 0,
                'confirmation_phrase' => '',
            ),
            array(
                'menu_id' => 2,
                'description' => 'Pay Loan',
                'next_menu_id' => 2,
                'step' => 0,
                'confirmation_phrase' => '',
            ),
            array(
                'menu_id' => 2,
                'description' => 'Check Loan Limit',
                'next_menu_id' => 2,
                'step' => 0,
                'confirmation_phrase' => '',
            ),
            array(
                'menu_id' => 3,
                'description' => 'Enter FirstName',
                'next_menu_id' => 0,
                'step' => 1,
                'confirmation_phrase' => 'FirstName',
            ),
            array(
                'menu_id' => 3,
                'description' => 'Enter Last Name',
                'next_menu_id' => 0,
                'step' => 2,
                'confirmation_phrase' => 'LastName',
            ),
            array(
                'menu_id' => 3,
                'description' => 'Enter ID Number',
                'next_menu_id' => 0,
                'step' => 3,
                'confirmation_phrase' => 'ID Number',
            ),
            array(
                'menu_id' => 3,
                'description' => 'Enter mpesa or airtel money number',
                'next_menu_id' => 0,
                'step' => 4,
                'confirmation_phrase' => 'Moneu Number',
            ),
//            array(
//                'menu_id' => 1,
//                'description' => 'Help',
//                'next_menu_id' => 7,
//                'step' => 0,
//                'confirmation_phrase' => '',
//            ),
//            array(
//                'menu_id' => 2,
//                'description' => 'Mini Statements',
//                'next_menu_id' => 7,
//                'step' => 0,
//                'confirmation_phrase' => '',
//            ),

        ));
    }
}
